package pl.uz.quizuz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pl.uz.quizuz.api.FirebaseAccessor;

/**
 * Game player's statistics class
 *
 * @author Mateusz Borowski
 */
public class GameStats extends AppCompatActivity {
    private TextView playedGamesTextView, wonGamesTextView,
            lostGamesTextView, correctAnswersTextView, incorrectAnswersTextView;
    private Button resetStatsButton;
    private FirebaseAccessor firebaseAccessor;
    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_stats);

        setView();
        updateStats();
    }

    /**
     * Sets all needed variables
     */
    private void setView() {
        firebaseAccessor = FirebaseAccessor.getInstance();
        compositeDisposable = new CompositeDisposable();

        playedGamesTextView = findViewById(R.id.playedGamesTextView);
        wonGamesTextView = findViewById(R.id.wonGamesTextView);
        lostGamesTextView = findViewById(R.id.lostGamesTextView);
        correctAnswersTextView = findViewById(R.id.correctAnswersTextView);
        incorrectAnswersTextView = findViewById(R.id.incorrectAnswersTextView);
        resetStatsButton = findViewById(R.id.resetStatsButton);
        resetStatsButton.setOnClickListener(view -> {
            firebaseAccessor.resetStats();
            updateStats();
        });
    }

    /**
     * Sets all textViews to current player's stats
     */
    private void updateStats() {
        final Disposable gamesPlayedSub = firebaseAccessor.getGamesPlayed()
                .subscribe(data -> playedGamesTextView.setText(String.valueOf(data)));
        compositeDisposable.add(gamesPlayedSub);

        final Disposable gamesWonSub = firebaseAccessor.getGamesWon()
                .subscribe(data -> wonGamesTextView.setText(String.valueOf(data)));
        compositeDisposable.add(gamesWonSub);

        final Disposable gamesLostSub = firebaseAccessor.getGamesLost()
                .subscribe(data -> lostGamesTextView.setText(String.valueOf(data)));
        compositeDisposable.add(gamesLostSub);

        final Disposable correctAnswersSub = firebaseAccessor.getCorrectAnswers()
                .subscribe(data -> correctAnswersTextView.setText(String.valueOf(data)));
        compositeDisposable.add(correctAnswersSub);

        final Disposable incorrectAnswersSub = firebaseAccessor.getIncorrectAnswers()
                .subscribe(data -> incorrectAnswersTextView.setText(String.valueOf(data)));
        compositeDisposable.add(incorrectAnswersSub);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
