package pl.uz.quizuz;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Space;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pl.uz.quizuz.api.FirebaseAccessor;

/**
 * Game categories class
 *
 * @author Mateusz Borowski
 */
public class GameCategories extends AppCompatActivity {

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_categories);

        populateScreen();
    }

    /**
     * Populates layout with categories from database
     */
    private void populateScreen() {
        compositeDisposable = new CompositeDisposable();

        //Gets linear layout to place all buttons in it
        final LinearLayout linearLayout = findViewById(R.id.categoriesLinearLayout);

        //Adds space between buttons
        Space space = new Space(this);
        space.setMinimumHeight(2);
        linearLayout.addView(space);

        final ArrayList<Integer> selectedIDs = new ArrayList<>();
        //All categories button
        final Button allCatButton = new Button(this);
        allCatButton.setText(getResources().getString(R.string.allCatButtonText));
        allCatButton.setHeight(200);
        allCatButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, GameMain.class);
            selectedIDs.add(0);
            intent.putExtra("categoryIDs", selectedIDs);
            startActivity(intent);
        });
        linearLayout.addView(allCatButton);

        space = new Space(this);
        space.setMinimumHeight(2);
        linearLayout.addView(space);

        final FirebaseAccessor firebaseAccessor = FirebaseAccessor.getInstance();
        final Disposable categoriesSubscription
                = firebaseAccessor.getCategories().subscribe(categoriesList -> {
            //Categories buttons
            final Button[] catButtons = new Button[categoriesList.size()];
            for (int i = 0; i < catButtons.length; i++) {
                catButtons[i] = new Button(this);
                catButtons[i].setText(categoriesList.get(i).getName());
                catButtons[i].setHeight(200);

                final int key = categoriesList.get(i).getId();
                Button tmp = catButtons[i];
                catButtons[i].setOnClickListener(view -> {
                    if (!selectedIDs.contains(key)) {
                        selectedIDs.add(key);
                        tmp.setBackgroundColor(Color.rgb(60, 60, 60));
                    } else {
                        selectedIDs.remove(Integer.valueOf(key));
                        tmp.setBackgroundColor(getResources().getColor(R.color.colorPrimary, getTheme()));
                    }
                });
                linearLayout.addView(catButtons[i]);

                final Space innerSpace = new Space(this);
                innerSpace.setMinimumHeight(2);
                linearLayout.addView(innerSpace);
            }
        });
        compositeDisposable.add(categoriesSubscription);

        final FloatingActionButton playSelectedCategoriesFAB = findViewById(R.id.playFAB);
        playSelectedCategoriesFAB.setOnClickListener(view -> {
            if (selectedIDs.isEmpty()) {
                Snackbar.make(view, "Wybierz pierw kategorie!", Snackbar.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(this, GameMain.class);
                intent.putExtra("categoryIDs", selectedIDs); //Passes chosen categoryIDs to new opened activity
                startActivity(intent); //Starts GameMain Activity
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}