package pl.uz.quizuz.api;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import pl.uz.quizuz.model.Category;
import pl.uz.quizuz.model.Question;

public class FirebaseAccessor {

    private final static String TAG = FirebaseAccessor.class.getCanonicalName();
    private static FirebaseAccessor INSTANCE;
    private DatabaseReference databaseReference;

    private FirebaseAccessor() {
        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    public static FirebaseAccessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FirebaseAccessor();
        }
        return INSTANCE;
    }

    /**
     * Gets categories
     *
     * @return observable returning list of categories
     */
    public Observable<List<Category>> getCategories() {
        return Observable.create(subscriber ->
                databaseReference.child("categories").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final List<Category> categories = new ArrayList<>();
                        dataSnapshot.getChildren().forEach(category ->
                                categories.add(category.getValue(Category.class)));
                        subscriber.onNext(categories);
                        subscriber.onComplete();
                        Log.i(TAG, "Categories fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Gets categories numgber
     *
     * @return observable returning a number of categories
     */
    public Observable<Integer> getCategoriesNumber() {
        return Observable.create(subscriber ->
                databaseReference.child("categories").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long count = dataSnapshot.getChildrenCount();
                        subscriber.onNext(Math.toIntExact(count));
                        subscriber.onComplete();
                        Log.i(TAG, "Categories number fetched   " + count);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Gets questions
     *
     * @return observable returning list of questions
     */
    public Observable<List<Question>> getQuestions() {
        return Observable.create(subscriber ->
                databaseReference.child("questions").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final List<Question> questions = new ArrayList<>();
                        dataSnapshot.getChildren().forEach(question ->
                                questions.add(question.getValue(Question.class)));
                        subscriber.onNext(questions);
                        subscriber.onComplete();
                        Log.i(TAG, "Questions fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Gets questions from given category ids
     *
     * @param categoryIds category identifier
     * @return observable returning list of questions from given category/ies
     */
    public Observable<List<Question>> getQuestions(final ArrayList<Integer> categoryIds) {
        return Observable.create(subscriber ->
                databaseReference.child("questions").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final List<Question> questions = new ArrayList<>();
                        dataSnapshot.getChildren().forEach(category ->
                                questions.add(category.getValue(Question.class)));
                        final List<Question> questionsByCategory = new ArrayList<>();
                        categoryIds.forEach(categoryId ->
                                questionsByCategory.addAll(questions
                                        .stream()
                                        .filter(question ->
                                                question.getCategoryId() == categoryId)
                                        .collect(Collectors.toList())));
                        subscriber.onNext(questionsByCategory);
                        subscriber.onComplete();
                        Log.i(TAG, MessageFormat.format("Questions from category id: {0} fetched",
                                categoryIds));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Increases number of correct answers
     */
    public void increaseCorrectAnswers() {
        databaseReference.child("statistics/correctAnswers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final long correctAnswers = (long) dataSnapshot.getValue();
                dataSnapshot.getRef().setValue(correctAnswers + 1);
                Log.i(TAG, "Correct answers incremented");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    /**
     * Increases number of incorrect answers
     */
    public void increaseIncorrectAnswers() {
        databaseReference.child("statistics/incorrectAnswers").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final long correctAnswers = (long) dataSnapshot.getValue();
                dataSnapshot.getRef().setValue(correctAnswers + 1);
                Log.i(TAG, "Incorrect answers incremented");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    /**
     * Gets number of correct answers
     *
     * @return observable returning a number of correct answers
     */
    public Observable<Integer> getCorrectAnswers() {
        return Observable.create(subscriber ->
                databaseReference.child("statistics/correctAnswers").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long correctAnswers = (long) dataSnapshot.getValue();
                        subscriber.onNext(Math.toIntExact(correctAnswers));
                        subscriber.onComplete();
                        Log.i(TAG, "Correct answers number fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Gets number of incorrect answers
     *
     * @return observable returning a number of incorrect answers
     */
    public Observable<Integer> getIncorrectAnswers() {
        return Observable.create(subscriber ->
                databaseReference.child("statistics/incorrectAnswers").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long incorrectAnswers = (long) dataSnapshot.getValue();
                        subscriber.onNext(Math.toIntExact(incorrectAnswers));
                        subscriber.onComplete();
                        Log.i(TAG, "Incorrect answers number fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Increases number of won games
     */
    public void increaseGamesWon() {
        databaseReference.child("statistics/gamesWon").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final long gamesWon = (long) dataSnapshot.getValue();
                dataSnapshot.getRef().setValue(gamesWon + 1);
                Log.i(TAG, "Games won incremented");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    /**
     * Increases number of lost games
     */
    public void increaseGamesLost() {
        databaseReference.child("statistics/gamesLost").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final long gamesLost = (long) dataSnapshot.getValue();
                dataSnapshot.getRef().setValue(gamesLost + 1);
                Log.i(TAG, "Games lost incremented");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    /**
     * Gets number of won games
     *
     * @return observable returning a number of games won
     */
    public Observable<Integer> getGamesWon() {
        return Observable.create(subscriber ->
                databaseReference.child("statistics/gamesWon").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long gamesWon = (long) dataSnapshot.getValue();
                        subscriber.onNext(Math.toIntExact(gamesWon));
                        subscriber.onComplete();
                        Log.i(TAG, "Games won number fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Gets number of lost games
     *
     * @return observable returning a number of games won
     */
    public Observable<Integer> getGamesLost() {
        return Observable.create(subscriber ->
                databaseReference.child("statistics/gamesLost").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long gamesLost = (long) dataSnapshot.getValue();
                        subscriber.onNext(Math.toIntExact(gamesLost));
                        subscriber.onComplete();
                        Log.i(TAG, "Games lost number fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Increases number of played games
     */
    public void increaseGamesPlayed() {
        databaseReference.child("statistics/gamesPlayed").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final long gamesPlayed = (long) dataSnapshot.getValue();
                dataSnapshot.getRef().setValue(gamesPlayed + 1);
                Log.i(TAG, "Games played incremented");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    /**
     * Gets number of played games
     *
     * @return observable returning a number of games played
     */
    public Observable<Integer> getGamesPlayed() {
        return Observable.create(subscriber ->
                databaseReference.child("statistics/gamesLost").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        final long gamesPlayed = (long) dataSnapshot.getValue();
                        subscriber.onNext(Math.toIntExact(gamesPlayed));
                        subscriber.onComplete();
                        Log.i(TAG, "Games played number fetched");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.wtf(TAG, "loadPost:onCancelled", databaseError.toException());
                    }
                })
        );
    }

    /**
     * Resets player's stats
     */
    public void resetStats() {
        databaseReference.child("statistics/gamesPlayed").setValue(0);
        databaseReference.child("statistics/gamesWon").setValue(0);
        databaseReference.child("statistics/gamesLost").setValue(0);
        databaseReference.child("statistics/correctAnswers").setValue(0);
        databaseReference.child("statistics/incorrectAnswers").setValue(0);
        Log.i(TAG, "Game statistics cleared");
    }
}
