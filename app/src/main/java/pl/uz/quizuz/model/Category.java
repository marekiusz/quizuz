package pl.uz.quizuz.model;

/**
 * Class that stores category from database
 * @author Mateusz Borowski
 */
public class Category {
    //Variables corresponding to those from database
    private int id;
    private String name;

    //Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
